exam-war-2018

[Содержание](CONTENTS.md)

[Литература](#1)

[Распределение](#21)

-----------------------------------

[Основы Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Дополнительные функции Markdown](https://docs.gitlab.com/ee/user/markdown.html)

В этот раз никаких заморочек с оформлением, настройкой окружения и LaTeX-ом. Пишем в markdown, который компилируется в HTML gitlab-ом. Очень тупо и просто. Можно даже на компьютер репозиторий не скачивать, а редактировать все прямо на сайте.
